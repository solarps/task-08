package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        DOMController domController = new DOMController(xmlFileName);
        domController.parse();
        List<Product> domResult = domController.getResult();
        System.out.println("DOM parser " + domResult);
        // sort (case 1)
        // PLACE YOUR CODE HERE

        // save
        String outputXmlFile = "output.dom.xml";
        domController.save(outputXmlFile);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        SAXController saxController = new SAXController(xmlFileName);
        saxController.parse();
        List<Product> saxResult = saxController.getResult();
        System.out.println("SAX parser " + saxResult);

        // save
        outputXmlFile = "output.sax.xml";
        saxController.save(outputXmlFile);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        STAXController staxController = new STAXController(xmlFileName);
        staxController.parse();
        List<Product> staxResult = staxController.getResult();
        System.out.println("STAX parser " + staxResult);

        // sort  (case 3)
        // PLACE YOUR CODE HERE

        // save
        outputXmlFile = "output.stax.xml";
        staxController.save(outputXmlFile);
    }

}
