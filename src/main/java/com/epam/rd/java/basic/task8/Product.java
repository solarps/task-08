package com.epam.rd.java.basic.task8;

public class Product {

    private int id;
    private String title;
    private int price;
    private int amount;

    public Product(int id, String title, int price, int amount) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", count=" + amount +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getPrice() {
        return price;
    }

    public int getAmount() {
        return amount;
    }
}
