package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Product;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;
    List<Product> result = new ArrayList<>();

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void parse() throws FileNotFoundException, XMLStreamException {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLEventReader reader = factory.createXMLEventReader(new FileInputStream(xmlFileName));
        int id = 0, price = 0, amount = 0;
        String title = null;
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                switch (startElement.getName().getLocalPart()) {
                    case "product":
                        Attribute attributeId = startElement.getAttributeByName(new QName("id"));
                        if (attributeId != null) {
                            id = Integer.parseInt(attributeId.getValue());
                        }
                        break;
                    case "title":
                        event = reader.nextEvent();
                        title = event.asCharacters().getData();
                        break;
                    case "price":
                        event = reader.nextEvent();
                        price = Integer.parseInt(event.asCharacters().getData());
                        break;
                    case "amount":
                        event = reader.nextEvent();
                        amount = Integer.parseInt(event.asCharacters().getData());
                        break;
                }
            }
            if (event.isEndElement()) {
                EndElement endElement = event.asEndElement();
                if (endElement.getName().getLocalPart().equals("product")) {
                    result.add(new Product(id, title, price, amount));
                }
            }
        }
    }


    public List<Product> getResult() {
        return result;
    }

    public void save(String xmlFileName) throws XMLStreamException, IOException {
        OutputStream outputStream = Files.newOutputStream(Path.of(xmlFileName));

        XMLOutputFactory outputFactory = XMLOutputFactory.newFactory();
        XMLStreamWriter writer = outputFactory.createXMLStreamWriter(outputStream,"utf-8");

        writer.writeStartDocument("UTF-8", "1.0");

        writer.writeStartElement("market");

        for (Product product : result) {
            writer.writeStartElement("product");
            writer.writeAttribute("id", String.valueOf(product.getId()));

            writer.writeStartElement("title");
            writer.writeCharacters(product.getTitle());
            writer.writeEndElement();

            writer.writeStartElement("price");
            writer.writeCharacters(String.valueOf(product.getPrice()));
            writer.writeEndElement();

            writer.writeStartElement("amount");
            writer.writeCharacters(String.valueOf(product.getAmount()));
            writer.writeEndElement();

            writer.writeEndElement();
        }
        writer.writeEndElement();

        writer.flush();
        writer.close();
    }

}