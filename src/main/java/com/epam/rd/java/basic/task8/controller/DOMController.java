package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Product;
import org.w3c.dom.*;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController  {

    List<Product> result = new ArrayList<>();

    private final String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void parse() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document document = builder.parse(xmlFileName);
        NodeList products = document.getElementsByTagName("product");

        for (int i = 0; i < products.getLength(); i++) {
            Element element = (Element) products.item(i);
            parseElement(element);
        }
    }

    private void parseElement(Element element) {
        int id = Integer.parseInt(element.getAttributes().getNamedItem("id").getNodeValue());
        String title = String.valueOf(element.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
        int price = Integer.parseInt(element.getElementsByTagName("price").item(0).getChildNodes().item(0).getNodeValue());
        int amount = Integer.parseInt(element.getElementsByTagName("amount").item(0).getChildNodes().item(0).getNodeValue());
        result.add(new Product(id, title, price, amount));
    }


    public List<Product> getResult() {
        return result;
    }

    public void save(String xmlFileName) throws ParserConfigurationException, FileNotFoundException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document document = builder.newDocument();


        Element root = document.createElement("market");
        for (Product product : result) {
            parseProduct(product, root, document);
        }

        document.appendChild(root);
        writeXMLByDOM(document, xmlFileName);
    }

    private void parseProduct(Product product, Element root, Document document) {
        Element pr = document.createElement("product");
        pr.setAttribute("id", String.valueOf(product.getId()));
        Element titleNode = document.createElement("title");
        Text title = document.createTextNode(product.getTitle());
        titleNode.appendChild(title);
        Element priceNode = document.createElement("price");
        Text price = document.createTextNode(String.valueOf(product.getPrice()));
        priceNode.appendChild(price);
        Element amountNode = document.createElement("amount");
        Text amount = document.createTextNode(String.valueOf(product.getAmount()));
        amountNode.appendChild(amount);
        root.appendChild(pr);
        pr.appendChild(titleNode);
        pr.appendChild(priceNode);
        pr.appendChild(amountNode);
    }


    private void writeXMLByDOM(Document document, String xmlFileName) throws FileNotFoundException {
        DOMImplementation imp = document.getImplementation();
        DOMImplementationLS impLS = (DOMImplementationLS) imp.getFeature("LS", "3.0");
        LSSerializer serializer = impLS.createLSSerializer();
        serializer.getDomConfig().setParameter("format-pretty-print", true);

        LSOutput out = impLS.createLSOutput();
        out.setEncoding("UTF-8");
        out.setByteStream(new FileOutputStream(xmlFileName));
        serializer.write(document, out);
    }
}
