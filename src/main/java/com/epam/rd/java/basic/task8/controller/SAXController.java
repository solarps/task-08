package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Product;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
    private final List<Product> result = new ArrayList<>();
    private final String xmlFileName;
    private String element;

    private int id;
    private String title;
    private int price;
    private int count;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void parse() {
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = parserFactory.newSAXParser();
            saxParser.parse(new File(xmlFileName), this);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void startElement(String namespace, String localName, String qName, Attributes attributes) {
        element = qName;
        if (element.equals("product")) {
            id = Integer.parseInt(attributes.getValue(0));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        element = "";
    }

    @Override
    public void characters(char[] chars, int start, int end) {
        if (element.equals("title")) {
            title = new String(chars, start, end);
        }
        if (element.equals("price")) {
            price = Integer.parseInt(new String(chars, start, end));
        }
        if (element.equals("amount")) {
            count = Integer.parseInt(new String(chars, start, end));
        }
        if (isFill()) {
            result.add(new Product(id, title, price, count));
            dropParameters();
        }
    }

    private void dropParameters() {
        id = 0;
        title = null;
        price = 0;
        count = 0;
    }

    private boolean isFill() {
        return id != 0 && title != null && price != 0 && count != 0;
    }

    public List<Product> getResult() {
        return result;
    }

    public void save(String xmlFileName) throws ParserConfigurationException, FileNotFoundException, TransformerException {
        writeXML(getSaveDocument(), xmlFileName);
    }

    private void parseProduct(Product product, Element root, Document document) {
        Element pr = document.createElement("product");
        pr.setAttribute("id", String.valueOf(product.getId()));
        Element titleNode = document.createElement("title");
        Text title = document.createTextNode(product.getTitle());
        titleNode.appendChild(title);
        Element priceNode = document.createElement("price");
        Text price = document.createTextNode(String.valueOf(product.getPrice()));
        priceNode.appendChild(price);
        Element amountNode = document.createElement("amount");
        Text amount = document.createTextNode(String.valueOf(product.getAmount()));
        amountNode.appendChild(amount);
        root.appendChild(pr);
        pr.appendChild(titleNode);
        pr.appendChild(priceNode);
        pr.appendChild(amountNode);

    }

    private void writeXML(Document document, String xmlFileName) throws TransformerException, FileNotFoundException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(new DOMSource(document), new StreamResult(new FileOutputStream(xmlFileName)));
    }

    private Document getSaveDocument() throws ParserConfigurationException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document document = builder.newDocument();


        Element root = document.createElement("market");
        for (Product product : result) {
            parseProduct(product, root, document);
        }

        document.appendChild(root);
        return document;
    }
}